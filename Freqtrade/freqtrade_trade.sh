git submodule init
git submodule update

cd freqtrade_eth
sudo docker-compose pull
sudo docker-compose up -d

cd freqtrade_btc
sudo docker-compose pull
sudo docker-compose up -d

# sudo docker-compose run --rm freqtrade download-data
# sudo docker-compose run --rm freqtrade backtesting --strategy-list BreakEven Diamond GodStra Heracles HourBasedStrategy MultiMa Strategy001 Strategy002 Strategy003 Strategy004 Strategy005 Supertrend Swing-High-To-Sky