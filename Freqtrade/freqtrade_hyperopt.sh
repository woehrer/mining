git submodule init
git submodule update


cd freqtrade_eth_hyperopt
docker-compose pull
docker-compose run --rm freqtrade download-data --timeframes 1m 5m 1h 4h --timerange 20200101-20220430  >> user_data/download.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy ADXMomentum -e 1000 >> user_data/ADXMomentum.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy AdxSmas -e 1000 >> user_data/AdxSmas.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy ASDTSRockwellTrading -e 1000 >> user_data/ASDTSRockwellTrading.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing stoploss --timerange 20200101-20220430 --strategy AverageStrategy -e 1000 >> user_data/AverageStrategy.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy BbandRsi -e 1000 >> user_data/BbandRsi.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy BinHV27 -e 1000 >> user_data/BinHV27.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing stoploss --timerange 20200101-20220430 --strategy BinHV45 -e 1000 >> user_data/BinHV45.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy CCIStrategy -e 1000 >> user_data/CCIStrategy.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy ClucMay72018 -e 1000 >> user_data/ClucMay72018.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy CMCWinner -e 1000 >> user_data/CMCWinner.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy CofiBitStrategy -e 1000 >> user_data/CofiBitStrategy.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy CombinedBinHAndCluc -e 1000 >> user_data/CombinedBinHAndCluc.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy DevilStra -e 1000 >> user_data/DevilStra.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy Diamond -e 1000 >> user_data/Diamond.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy EMASkipPump -e 1000 >> user_data/EMASkipPump.log
# docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy GodStra -e 1000 >> user_data/GodStra.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy GodStraNew -e 1000 >> user_data/GodStraNew.log
# docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy Heracles -e 1000 >> user_data/Heracles.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy hlhb -e 1000 >> user_data/hlhb.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy Low_BB -e 1000 >> user_data/Low_BB.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy mabStra -e 1000 >> user_data/mabStra.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing sell stoploss --timerange 20200101-20220430 --strategy MACDStrategy_crossed -e 1000 >> user_data/MACDStrategy_crossed.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing stoploss --timerange 20200101-20220430 --strategy MACDStrategy -e 1000 >> user_data/MACDStrategy.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy MultiMa -e 1000 >> user_data/MultiMa.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy MultiRSI -e 1000 >> user_data/MultiRSI.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy Quickie -e 1000 >> user_data/Quickie.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy ReinforcedAverageStrategy -e 1000 >> user_data/ReinforcedAverageStrategy.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy ReinforcedQuickie -e 1000 >> user_data/ReinforcedQuickie.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy ReinforcedSmoothScalp -e 1000 >> user_data/ReinforcedSmoothScalp.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy Scalp -e 1000 >> user_data/Scalp.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy Simple -e 1000 >> user_data/Simple.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy SmoothOperator -e 1000 >> user_data/SmoothOperator.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy SmoothScalp -e 1000 >> user_data/SmoothScalp.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy Strategy001 -e 1000 >> user_data/Strategy001.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy Strategy002 -e 1000 >> user_data/Strategy002.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy Strategy003 -e 1000 >> user_data/Strategy003.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy Strategy004 -e 1000 >> user_data/Strategy004.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy Strategy005 -e 1000 >> user_data/Strategy005.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy Supertrend -e 1000 >> user_data/Supertrend.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy SwingHighToSky -e 1000 >> user_data/SwingHighToSky.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy TDSequentialStrategy -e 1000 >> user_data/TDSequentialStrategy.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy wtc -e 1000 >> user_data/wtc.log
#docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy Zeus -e 1000 >> user_data/Zeus.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy DoubleEMACrossoverWithTrend -e 1000 >> user_data/DoubleEMACrossoverWithTrend.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy EMAPriceCrossoverWithThreshold -e 1000 >> user_data/EMAPriceCrossoverWithThreshold.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy MACDCrossoverWithTrend -e 1000 >> user_data/EMAPriceCrossoverWithThreshold.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy RSIDirectionalWithTrend -e 1000 >> user_data/RSIDirectionalWithTrend.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy RSIDirectionalWithTrendSlow -e 1000 >> user_data/RSIDirectionalWithTrendSlow.log

docker-compose run --rm freqtrade backtesting --timerange 20200101-20220430 --strategy-list ADXMomentum AdxSmas ASDTSRockwellTrading AverageStrategy BbandRsi BinHV27 BinHV45 CCIStrategy ClucMay72018 CMCWinner CofiBitStrategy CombinedBinHAndCluc DevilStra Diamond EMASkipPump GodStraNew hlhb Low_BB mabStra MACDStrategy_crossed MACDStrategy MultiMa MultiRSI Quickie ReinforcedAverageStrategy ReinforcedQuickie ReinforcedSmoothScalp Scalp Simple SmoothOperator SmoothScalp Strategy001 Strategy002 Strategy003 Strategy004 Strategy005 Supertrend SwingHighToSky TDSequentialStrategy wtc DoubleEMACrossoverWithTrend EMAPriceCrossoverWithThreshold MACDCrossoverWithTrend RSIDirectionalWithTrend RSIDirectionalWithTrendSlow >> user_data/backtesting.log

cd ..

cd freqtrade_btc_hyperopt
docker-compose pull
docker-compose run --rm freqtrade download-data --timeframes 1m 5m 1h 4h --timerange 20200101-20220430  >> user_data/download.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy ADXMomentum -e 1000 >> user_data/ADXMomentum.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy AdxSmas -e 1000 >> user_data/AdxSmas.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy ASDTSRockwellTrading -e 1000 >> user_data/ASDTSRockwellTrading.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing stoploss --timerange 20200101-20220430 --strategy AverageStrategy -e 1000 >> user_data/AverageStrategy.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy BbandRsi -e 1000 >> user_data/BbandRsi.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy BinHV27 -e 1000 >> user_data/BinHV27.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing stoploss --timerange 20200101-20220430 --strategy BinHV45 -e 1000 >> user_data/BinHV45.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy CCIStrategy -e 1000 >> user_data/CCIStrategy.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy ClucMay72018 -e 1000 >> user_data/ClucMay72018.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy CMCWinner -e 1000 >> user_data/CMCWinner.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy CofiBitStrategy -e 1000 >> user_data/CofiBitStrategy.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy CombinedBinHAndCluc -e 1000 >> user_data/CombinedBinHAndCluc.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy DevilStra -e 1000 >> user_data/DevilStra.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy Diamond -e 1000 >> user_data/Diamond.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy EMASkipPump -e 1000 >> user_data/EMASkipPump.log
# docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy GodStra -e 1000 >> user_data/GodStra.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy GodStraNew -e 1000 >> user_data/GodStraNew.log
# docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy Heracles -e 1000 >> user_data/Heracles.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy hlhb -e 1000 >> user_data/hlhb.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy Low_BB -e 1000 >> user_data/Low_BB.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy mabStra -e 1000 >> user_data/mabStra.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing sell stoploss --timerange 20200101-20220430 --strategy MACDStrategy_crossed -e 1000 >> user_data/MACDStrategy_crossed.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing stoploss --timerange 20200101-20220430 --strategy MACDStrategy -e 1000 >> user_data/MACDStrategy.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy MultiMa -e 1000 >> user_data/MultiMa.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy MultiRSI -e 1000 >> user_data/MultiRSI.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy Quickie -e 1000 >> user_data/Quickie.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy ReinforcedAverageStrategy -e 1000 >> user_data/ReinforcedAverageStrategy.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy ReinforcedQuickie -e 1000 >> user_data/ReinforcedQuickie.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy ReinforcedSmoothScalp -e 1000 >> user_data/ReinforcedSmoothScalp.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy Scalp -e 1000 >> user_data/Scalp.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy Simple -e 1000 >> user_data/Simple.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy SmoothOperator -e 1000 >> user_data/SmoothOperator.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy SmoothScalp -e 1000 >> user_data/SmoothScalp.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy Strategy001 -e 1000 >> user_data/Strategy001.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy Strategy002 -e 1000 >> user_data/Strategy002.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy Strategy003 -e 1000 >> user_data/Strategy003.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy Strategy004 -e 1000 >> user_data/Strategy004.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy Strategy005 -e 1000 >> user_data/Strategy005.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy Supertrend -e 1000 >> user_data/Supertrend.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy SwingHighToSky -e 1000 >> user_data/SwingHighToSky.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy TDSequentialStrategy -e 1000 >> user_data/TDSequentialStrategy.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy wtc -e 1000 >> user_data/wtc.log
#docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces buy roi trailing sell stoploss --timerange 20200101-20220430 --strategy Zeus -e 1000 >> user_data/Zeus.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy DoubleEMACrossoverWithTrend -e 1000 >> user_data/DoubleEMACrossoverWithTrend.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy EMAPriceCrossoverWithThreshold -e 1000 >> user_data/EMAPriceCrossoverWithThreshold.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy MACDCrossoverWithTrend -e 1000 >> user_data/EMAPriceCrossoverWithThreshold.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy RSIDirectionalWithTrend -e 1000 >> user_data/RSIDirectionalWithTrend.log
docker-compose run --rm freqtrade hyperopt --hyperopt-loss ShortTradeDurHyperOptLoss --spaces roi trailing stoploss --timerange 20200101-20220430 --strategy RSIDirectionalWithTrendSlow -e 1000 >> user_data/RSIDirectionalWithTrendSlow.log

docker-compose run --rm freqtrade backtesting --timerange 20200101-20220430 --strategy-list ADXMomentum AdxSmas ASDTSRockwellTrading AverageStrategy BbandRsi BinHV27 BinHV45 CCIStrategy ClucMay72018 CMCWinner CofiBitStrategy CombinedBinHAndCluc DevilStra Diamond EMASkipPump GodStraNew hlhb Low_BB mabStra MACDStrategy_crossed MACDStrategy MultiMa MultiRSI Quickie ReinforcedAverageStrategy ReinforcedQuickie ReinforcedSmoothScalp Scalp Simple SmoothOperator SmoothScalp Strategy001 Strategy002 Strategy003 Strategy004 Strategy005 Supertrend SwingHighToSky TDSequentialStrategy wtc DoubleEMACrossoverWithTrend EMAPriceCrossoverWithThreshold MACDCrossoverWithTrend RSIDirectionalWithTrend RSIDirectionalWithTrendSlow >> user_data/backtesting.log

cd ..


# sudo docker-compose run --rm freqtrade download-data
# sudo docker-compose run --rm freqtrade backtesting --timerange 20200101-20220430 --strategy-list BreakEven Diamond GodStra Heracles HourBasedStrategy MultiMa Strategy001 Strategy002 Strategy003 Strategy004 Strategy005 Supertrend Swing-High-To-Sky