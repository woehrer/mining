git submodule init
git submodule update

# Freqtrade ETH
cp -a freqtrade-strategies/user_data/strategies/. freqtrade_eth/user_data/strategies/
cp -a freqtrade-strategies/user_data/strategies/berlinguyinca/. freqtrade_eth/user_data/strategies/
cp -a freqtrade-strategies/user_data/strategies/lookahead_bias/. freqtrade_eth/user_data/strategies/
cp -a freqtrade-strategies-that-work/*.py freqtrade_eth/user_data/strategies/
cp -r freqtrade-strategies/user_data/hyperopts/ freqtrade_eth/user_data/

# Freqtrade BTC
cp -a freqtrade-strategies/user_data/strategies/. freqtrade_btc/user_data/strategies/
cp -a freqtrade-strategies/user_data/strategies/berlinguyinca/. freqtrade_btc/user_data/strategies/
cp -a freqtrade-strategies/user_data/strategies/lookahead_bias/. freqtrade_btc/user_data/strategies/
cp -a freqtrade-strategies-that-work/*.py freqtrade_btc/user_data/strategies/
cp -r freqtrade-strategies/user_data/hyperopts/ freqtrade_btc/user_data/

# Freqtrade ETH Hyperopt
cp -a freqtrade-strategies/user_data/strategies/. freqtrade_eth_hyperopt/user_data/strategies/
cp -a freqtrade-strategies/user_data/strategies/berlinguyinca/. freqtrade_eth_hyperopt/user_data/strategies/
cp -a freqtrade-strategies/user_data/strategies/lookahead_bias/. freqtrade_eth_hyperopt/user_data/strategies/
cp -a freqtrade-strategies-that-work/*.py freqtrade_eth_hyperopt/user_data/strategies/
cp -r freqtrade-strategies/user_data/hyperopts/ freqtrade_eth_hyperopt/user_data/

# Freqtrade BTC Hyperopt
cp -a freqtrade-strategies/user_data/strategies/. freqtrade_btc_hyperopt/user_data/strategies/
cp -a freqtrade-strategies/user_data/strategies/berlinguyinca/. freqtrade_btc_hyperopt/user_data/strategies/
cp -a freqtrade-strategies/user_data/strategies/lookahead_bias/. freqtrade_btc_hyperopt/user_data/strategies/
cp -a freqtrade-strategies-that-work/*.py freqtrade_btc_hyperopt/user_data/strategies/
cp -r freqtrade-strategies/user_data/hyperopts/ freqtrade_btc_hyperopt/user_data/



cd freqtrade_btc
docker-compose pull
cd ..

cd freqtrade_btc_hyperopt
docker-compose pull
cd ..

cd freqtrade_eth_hyperopt
docker-compose pull
cd ..

cd freqtrade_eth
docker-compose pull


# sudo docker-compose run --rm freqtrade download-data
# sudo docker-compose run --rm freqtrade backtesting --strategy-list BreakEven Diamond GodStra Heracles HourBasedStrategy MultiMa Strategy001 Strategy002 Strategy003 Strategy004 Strategy005 Supertrend Swing-High-To-Sky