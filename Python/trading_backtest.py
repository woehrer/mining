import os
import warnings
import csv
import multiprocessing

import pandas as pd

import helper.binance
import helper.config
import helper.functions
import helper.signals
import helper.backtest

warnings.filterwarnings('ignore')

helper.functions.initlogger("trading_backtest.log")

conf = helper.config.initconfig()

import helper.binance

answer = helper.binance.get_24h_ticker()

## Search the Pairs

CurrencyPairList = []
for list in answer:
    if 'USDT' in list['symbol'][-4:]:
        if float(list['volume']) > 0.0:
            CurrencyPairList.append(list['symbol'])

## Create the Data Folder

if not os.path.exists('./Data'):
        os.makedirs('./Data')


## Prepair the Multiprocessing

## Download the Data
if conf['download'] == 'True':
    for CurrencyPair in CurrencyPairList:
        if not os.path.exists('./Data/CurrencyPair_{}'.format(CurrencyPair)):
            os.makedirs('./Data/CurrencyPair_{}'.format(CurrencyPair))

        print("Request Data " + CurrencyPair)

        data = helper.binance.get_historical_klines(CurrencyPair)

        # Start to Save Data
        # Open time / Open / High / Low / Close / Volume / Close time / Quote asset volume / Number of trades / Taker buy base asset volume / Taker buy quote asset volume / Ignore
        print("Save Data " + CurrencyPair)
        with open('./Data/CurrencyPair_{}/Data.csv'.format(CurrencyPair), 'w', newline = '') as f:
            writer = csv.writer(f)
            writer.writerow(['Open time', 'open', 'high', 'low', 'close', 'volume', 'Close time', 'Quote asset volume', 'Number of trades', 'Taker buy base asset volume', 'Taker buy quote asset volume', 'Ignore'])
            for line in data:
                writer.writerow(line)


## Prepair Data
if conf['prepair'] == 'True':
    print("Number of cpu : ", multiprocessing.cpu_count())

    p = multiprocessing.Pool(int(multiprocessing.cpu_count()-2))

    for CurrencyPair in CurrencyPairList:

        p.apply_async(helper.signals.multi_prepair(CurrencyPair), args=(CurrencyPair))
    p.close()
    p.join()


## Backtest

if conf['backtesting'] == 'True':
    
    for CurrencyPair in CurrencyPairList:
        print("Backtesting: " + CurrencyPair)
        helper.backtest.backtest(CurrencyPair)
