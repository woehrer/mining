import logging
import time
import schedule
import matplotlib.pyplot as plt
import matplotlib.dates as mpl_dates
import pandas as pd
import os
import shutil

import helper.mysql
import helper.config
import helper.telegramsend
import helper.freqtradeAPI
import helper.functions
import helper.twitter
import helper.binance

conf = helper.config.initconfig()

helper.functions.initlogger("statistiker.log")

schedule.every().day.at("00:00").do(send_Telegram_message)
schedule.every().day.at("23:00").do(send_twitter_message)
schedule.every().day.at("09:00").do(check24hchange)

while True:
    try:

        # Start Schedule
        schedule.run_pending()

        time.sleep(120)

    except Exception as e:
        logging.error("Fehler beim schedule in Statistiker: " + str(e))
        print("Fehler beim schedule in Statistiker: " + str(e))
