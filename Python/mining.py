import os
import time
import subprocess
import signal
import logging

import helper.functions
import helper.sensors
import helper.config

#Inizialisierung der Konfiguration
conf = helper.config.initconfig()

helper.functions.initlogger("mining.log")

def start_mining():
    Subprogram = subprocess.Popen(['~/ethminer/bin/ethminer -G -P stratum1+tcp://' + conf['eth_walletid'] + '@eth.2miners.com:2020 --cl-devices 1 --response-timeout 200'], stdout=subprocess.PIPE, shell = True) 
    return Subprogram

def fake_process():
    Subprogram = subprocess.Popen(['sleep', '1'], stdout=subprocess.PIPE, shell = True) 
    return Subprogram

if conf['activemining1'] == 'True':
    Subprogram = start_mining()
else:
    Subprogram = fake_process()


while True:
    try:
        # Check Config
        conf = helper.config.initconfig()

        os.system('clear')
        helper.sensors.check()

        # Check if Subprocess is still running
        if Subprogram.poll() is not None and conf['activemining1'] == 'True':
            Subprogram = start_mining()
            print('Mining 1 restarted')
            logging.info('Mining 1 restarted')
        elif Subprogram.poll() is not None and conf['activemining1'] == 'False':
            print('Mining 1 stopped')
            logging.info('Mining 1 should not running')
        elif Subprogram.poll() is None and conf['activemining1'] == 'False':
            Subprogram.kill()
            print('Mining 1 stopped')
            logging.info('Mining 1 stopped')
        else:
            print('Mining 1 is running')

        time.sleep(60)
    except KeyboardInterrupt:
        Subprogram.kill()
        print("\n\nProgramm wird beendet...")
        break
    except Exception as e:
        logging.error("Fehler beim senden des Telegram-Nachrichten: " + str(e))
        print("Fehler beim senden des Telegram-Nachrichten: " + str(e))
        Subprogram.kill()

