import os
import warnings

import helper.binance
import helper.training
import helper.config

warnings.filterwarnings('ignore')

conf = helper.config.initconfig()



CurrencyPairList = ["BTCUSDT","ETHUSDT","BNBUSDT","XMRUSDT","ATOMUSDT","LUNCUSDT","ADAUSDT","AMBUSDT","XRPUSDT","SOLUSDT","DOGEUSDT","DOTUSDT","MATICUSDT","ERNUSDT","SHIBUSDT","SUNUSDT","LUNAUSDT"]


if not os.path.exists('./KI'):
        os.makedirs('./KI')

if not os.path.exists('./KI/Data'):
        os.makedirs('./KI/Data')

if not os.path.exists('./KI/Actual'):
        os.makedirs('./KI/Actual')

if not os.path.exists('./KI/Models'):
        os.makedirs('./KI/Models')

if not os.path.exists('./KI/Results'):
        os.makedirs('./KI/Results')

for CurrencyPair in CurrencyPairList:
    if not os.path.exists('./KI/Data/CurrencyPair_{}'.format(CurrencyPair)):
        os.makedirs('./KI/Data/CurrencyPair_{}'.format(CurrencyPair))

    if not os.path.exists('./KI/Actual/CurrencyPair_{}'.format(CurrencyPair)):
        os.makedirs('./KI/Actual/CurrencyPair_{}'.format(CurrencyPair))

CurrencyPairList = ["BTCUSDT","ETHUSDT","BNBUSDT","XMRUSDT","ATOMUSDT","LUNCUSDT","ADAUSDT","AMBUSDT","XRPUSDT","SOLUSDT","DOGEUSDT","DOTUSDT","MATICUSDT","ERNUSDT","SHIBUSDT","SUNUSDT","LUNAUSDT"]

if conf['backtesting'] == 'True':
    helper.training.backtesting(CurrencyPairList)

if conf['check'] == 'True':
    helper.training.check(CurrencyPairList)
