import json
import os
import shutil

def handler(data):
    if not os.path.isfile('./Python/strategies/strategie.json'):
        shutil.copyfile('./Python/strategies/strategie.json.example','./Python/strategies/strategie.json')

    return RSI(data)



def RSI(data):
    f = open("./Python/strategies/strategie.json")
    file = json.load(f) # TODO
    if  (float(data['rsi_6'].iloc[-2:-1]) < file['RSI']['1'] and \
        float(data['sma_6'].iloc[-2:-1]) > float(data['close'].iloc[-2:-1])) or \
        (float(data['rsi_12'].iloc[-2:-1]) < file['RSI']['2'] and \
        float(data['sma_12'].iloc[-2:-1]) > float(data['close'].iloc[-2:-1])) or \
        (float(data['rsi_24'].iloc[-2:-1]) < file['RSI']['3'] and \
        float(data['sma_24'].iloc[-2:-1]) > float(data['close'].iloc[-2:-1])) or \
        (float(data['rsi_200'].iloc[-2:-1]) < file['RSI']['4'] and \
        float(data['sma_200'].iloc[-2:-1]) > float(data['close'].iloc[-2:-1])):
        return True
    return False

