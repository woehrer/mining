from flask import Flask , render_template, request

import helper.sqlite
import helper.functions
import helper.config
import helper.binance
import helper.trade

import logging
import os
import datetime
import numpy as np

app = Flask(__name__)

logger = helper.functions.initlogger("flask_run.log")

conf = helper.config.initconfig()

print("Flask start")
logging.info("Flask start")

def Sort(sub_li):
    sub_li.sort(key = lambda x: x[17],reverse=True)
    return sub_li



@app.route('/')
def index():
    binanceprice = helper.binance.get_24h_ticker()
    USDT = float(helper.binance.get_balance()['free'])
    totalUSDT = 0.0
    totalprofitUSDT = 0.0
    BTC = {}
    ETH = {}
    BNB = {}
    PAXG = {}
    BTC['total'] = 0.0
    ETH['total'] = 0.0
    BNB['total'] = 0.0
    PAXG['total'] = 0.0
    BTC['USDT'] = 0.0
    ETH['USDT'] = 0.0
    BNB['USDT'] = 0.0
    PAXG['USDT'] = 0.0
    BTC['portion'] = 0.0
    ETH['portion'] = 0.0
    BNB['portion'] = 0.0
    PAXG['portion'] = 0.0
    BTC['price'] = 0.0
    ETH['price'] = 0.0
    BNB['price'] = 0.0
    PAXG['price'] = 0.0
    BTC['arr'] = []
    BTC['arr_w'] = []
    ETH['arr'] = []
    ETH['arr_w'] = []
    BNB['arr'] = []
    BNB['arr_w'] = []
    PAXG['arr'] = []
    PAXG['arr_w'] = []

    allopenBuys = helper.sqlite.getSearchNewBuys()
    allBuys2 = []
    for Buy in allopenBuys:
        Buy=list(Buy)
        Buy[4] = datetime.datetime.fromtimestamp(Buy[4]/1000).strftime("%d.%m.%Y %H:%M:%S")
        allBuys2.append(Buy)
        totalUSDT += Buy[8]


    allopenSells = helper.sqlite.getSearchNewSells()
    allSells2 = []
    for Sell in allopenSells:
        Sell=list(Sell)
        Sell[4] = datetime.datetime.fromtimestamp(Sell[4]/1000).strftime("%d.%m.%Y %H:%M:%S")
        allSells2.append(Sell)
        totalUSDT += Sell[8]


    allopenTrades = helper.sqlite.getSearchFilledBuysall()
    allTrades2 = []
    
    
    for Trade in allopenTrades:
        Trade = list(Trade)
        # Sum of Coins
        if Trade[0] == "BTCUSDT":
            BTC['total'] += Trade[7]
            BTC['USDT'] += Trade[8]
            BTC['arr'] = np.append(BTC['arr'],Trade[5])
            BTC['arr_w'] = np.append(BTC['arr_w'],Trade[6])
        if Trade[0] == "ETHUSDT":
            ETH['total'] += Trade[7]
            ETH['USDT'] += Trade[8]
            ETH['arr'] = np.append(ETH['arr'],Trade[5])
            ETH['arr_w'] = np.append(ETH['arr_w'],Trade[6])
        if Trade[0] == "BNBUSDT":
            BNB['total'] += Trade[7]
            BNB['USDT'] += Trade[8]
            BNB['arr'] = np.append(BNB['arr'],Trade[5])
            BNB['arr_w'] = np.append(BNB['arr_w'],Trade[6])
        if Trade[0] == "PAXGUSDT":
            PAXG['total'] += Trade[7]
            PAXG['USDT'] += Trade[8]
            PAXG['arr'] = np.append(PAXG['arr'],Trade[5])
            PAXG['arr_w'] = np.append(PAXG['arr_w'],Trade[6])

        for dict in binanceprice:
            if dict['symbol'] == Trade[0]:
                price = float(dict['lastPrice'])
        profit = float(((price/Trade[5])*100)-100)
        profitUSDT = float(Trade[8])*(profit/100+1)-float(Trade[8])
        totalprofitUSDT += profitUSDT
        totalUSDT += Trade[8]
        Trade.append(round(profit,2))
        Trade.append(round(profitUSDT,2))
        Trade[4] = datetime.datetime.fromtimestamp(Trade[4]/1000).strftime("%d.%m.%Y %H:%M:%S")
        if Trade[14]:
            Trade[14] = str(round(float(Trade[14]),2))
        allTrades2.append(Trade)
    allTrades2 = Sort(allTrades2)

    USDT = round(USDT,2)
    totalprofitUSDT = round(totalprofitUSDT,2)
    totalUSDT += totalprofitUSDT + USDT
    totalUSDT = round(totalUSDT,2)

    BTC['total'] = round(BTC['total'],4)
    ETH['total'] = round(ETH['total'],3)
    BNB['total'] = round(BNB['total'],3)
    PAXG['total'] = round(PAXG['total'],3)

    BTC['USDT'] = round(BTC['USDT'],2)
    ETH['USDT'] = round(ETH['USDT'],2)
    BNB['USDT'] = round(BNB['USDT'],2)
    PAXG['USDT'] = round(PAXG['USDT'],2)

    BTC['portion'] = BTC['USDT']/ (totalUSDT - USDT - totalprofitUSDT) * 100.0
    ETH['portion'] = ETH['USDT']/ (totalUSDT - USDT - totalprofitUSDT) * 100.0
    BNB['portion'] = BNB['USDT']/ (totalUSDT - USDT - totalprofitUSDT) * 100.0
    PAXG['portion'] = PAXG['USDT']/ (totalUSDT - USDT - totalprofitUSDT) * 100.0

    BTC['portion'] = round(BTC['portion'],1)
    ETH['portion'] = round(ETH['portion'],1)
    BNB['portion'] = round(BNB['portion'],1)
    PAXG['portion'] = round(PAXG['portion'],1)

    for dict in binanceprice:
        if dict['symbol'] == 'BTCUSDT':
            BTC['price'] = float(dict['lastPrice'])
        if dict['symbol'] == 'ETHUSDT':
            ETH['price'] = float(dict['lastPrice'])
        if dict['symbol'] == 'BNBUSDT':
            BNB['price'] = float(dict['lastPrice'])
        if dict['symbol'] == 'PAXGUSDT':
            PAXG['price'] = float(dict['lastPrice'])

    if len(BTC['arr']) > 0 and len(BTC['arr_w']) > 0:
        gewichteter_durchschnitt = np.average(BTC['arr'],weights = BTC['arr_w'])
        BTC['profit'] = profit = float(((BTC['price']/gewichteter_durchschnitt)*100)-100)
    else:
        BTC['profit'] = 0.0

    if len(ETH['arr']) > 0 and len(ETH['arr_w']) > 0:
        gewichteter_durchschnitt = np.average(ETH['arr'],weights = ETH['arr_w'])
        ETH['profit'] = profit = float(((ETH['price']/gewichteter_durchschnitt)*100)-100)
    else:
        ETH['profit'] = 0.0

    if len(BNB['arr']) > 0 and len(BNB['arr_w']) > 0:
        gewichteter_durchschnitt = np.average(BNB['arr'],weights = BNB['arr_w'])
        BNB['profit'] = profit = float(((BNB['price']/gewichteter_durchschnitt)*100)-100)
    else:
        BNB['profit'] = 0.0

    if len(PAXG['arr']) > 0 and len(PAXG['arr_w']) > 0:
        gewichteter_durchschnitt = np.average(PAXG['arr'],weights = PAXG['arr_w'])
        PAXG['profit'] = profit = float(((PAXG['price']/gewichteter_durchschnitt)*100)-100)
    else:
        PAXG['profit'] = 0.0

    BTC['profit'] = round(BTC['profit'],2)
    ETH['profit'] = round(ETH['profit'],2)
    BNB['profit'] = round(BNB['profit'],2)
    PAXG['profit'] = round(PAXG['profit'],2)
    

    return render_template('index.html',    allopenTrades=allTrades2, 
                                            allopenBuys=allBuys2, 
                                            allopenSells=allSells2, 
                                            USDT=USDT, 
                                            totalprofitUSDT=totalprofitUSDT,
                                            totalUSDT=totalUSDT,
                                            BTC=BTC,
                                            ETH=ETH,
                                            BNB=BNB,
                                            PAXG=PAXG)


@app.route('/trade')
def trade():
    Id = request.args.get('Id')
    Trade = helper.sqlite.getSearchorderId2(Id)
    Trade = list(Trade)
    Trade[4] = datetime.datetime.fromtimestamp(Trade[4]/1000).strftime("%d.%m.%Y %H:%M:%S")
    binanceprice = helper.binance.get_24h_ticker()
    for dict in binanceprice:
        if dict['symbol'] == Trade[0]:
            price = float(dict['lastPrice'])
    profit = float(((price/Trade[5])*100)-100)
    Trade.append(str(round(profit,2)))
    return render_template('trade.html', Trade=Trade)


@app.route('/sell')
def sell():
    Id = request.args.get('Id')
    helper.trade.sell2(Id)
    return "<h1>SELL {}".format(Id)

@app.route('/buy')
def buy():
    Pair = request.args.get('Pair')
    helper.trade.buy(Pair)
    return "<h1>BUY {}".format(Pair)

@app.errorhandler(404)
def page_not_found(e):
    return "<h1>404</h1><p>The resource could not be found.</p>", 404


app.run(host='0.0.0.0', debug=True, port=5001)
