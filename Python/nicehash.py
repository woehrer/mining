import helper.nicehashAPI
import helper.mysql
import helper.telegramsend
import time
import helper.functions
import logging

logger = helper.functions.initlogger("nicehash.log")

helper.mysql.init()

print("Start Nicehash")
logging.info("Start nicehash.py")

while True:

    account = helper.nicehashAPI.request('/main/api/v2/accounting/accounts2')
    print(account['total'])
    data = helper.nicehashAPI.request('/main/api/v2/mining/rigs')

    # print(data)
    sqldata = {}
    print("totalRigs: ", data['totalRigs'])

    sqldata = {}
    print("totalProfitability: ", data['totalProfitability'])
    sqldata['profitability'] = data['totalProfitability']
    helper.mysql.insertNicehashProfitability(sqldata)

    sqldata = {}
    print("rigId: ", data['miningRigs'][0]['rigId'])
    sqldata['device'] = 0
    print("device 0 temperature: ",data['miningRigs'][0]['devices'][0]['temperature'])
    sqldata['temperature'] = data['miningRigs'][0]['devices'][0]['temperature']
    print("device 0 load: ",data['miningRigs'][0]['devices'][0]['load'])
    sqldata['load'] = data['miningRigs'][0]['devices'][0]['load']
    print("device 0 power: ",data['miningRigs'][0]['devices'][0]['powerUsage'])
    sqldata['powerUsage'] = data['miningRigs'][0]['devices'][0]['powerUsage']

    helper.mysql.insertNicehashDevices(sqldata)

    sqldata = {}
    sqldata['device'] = 1
    print("device 1 temperature: ",data['miningRigs'][0]['devices'][1]['temperature'])
    sqldata['temperature'] = data['miningRigs'][0]['devices'][1]['temperature']
    print("device 1 load: ",data['miningRigs'][0]['devices'][1]['load'])
    sqldata['load'] = data['miningRigs'][0]['devices'][1]['load']
    print("device 1 power: ",data['miningRigs'][0]['devices'][1]['powerUsage'])
    sqldata['powerUsage'] = data['miningRigs'][0]['devices'][1]['powerUsage']

    helper.mysql.insertNicehashDevices(sqldata)

    result = helper.mysql.requestsolarpower()

    x = 0.0
    for i in result:
        if None in i:
            break
            logging.error("None in Result")
        x += float(i[0])

    avg = x / len(result)

    print(avg)

    if avg < -270 and (data['miningRigs'][0]['devices'][0]['status']['enumName'] == 'INACTIVE' or data['miningRigs'][0]['devices'][1]['status']['enumName'] == 'INACTIVE'):
        helper.telegramsend.send("Start Mining")
        print("START MINING")
        logging.info("Start Mining")
        body = {
        "action": "START",
        "rigId": "0-Cgxe2KWZUXOFnDSnEjMoUA"
        }

        helper.nicehashAPI.request_post('/main/api/v2/mining/rigs/status2', body)

    if avg > 50 and (data['miningRigs'][0]['devices'][0]['status']['enumName'] == 'MINING' or data['miningRigs'][0]['devices'][1]['status']['enumName'] == 'MINING'):
        helper.telegramsend.send("Stop Mining")
        print("STOP MINING")
        logging.info("Stop Mining")
        body = {
        "action": "STOP",
        "rigId": "0-Cgxe2KWZUXOFnDSnEjMoUA"
        }

        helper.nicehashAPI.request_post('/main/api/v2/mining/rigs/status2', body)

    time.sleep(60)