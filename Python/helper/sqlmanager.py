import sqlalchemy as db

metadata = db.MetaData()
Money = db.Table('Money', metadata,
            db.Column('Id', db.Integer(),primary_key = True),
            db.Column('oldMoney', db.Float, nullable = False),
            db.Column('newMoney', db.Float, nullable = False)
            )

Trades = db.Table('Trades', metadata,
            db.Column('Id', db.Integer(),primary_key = True),
            db.Column('price', db.Float, nullable = False),
            db.Column('Qty',db.Float, nullable = False),
            db.Column('Status',db.String, nullable = False),
            db.Column('Kind', db.String, nullable = False),
            db.Column('Trailing', db.Float, nullable = True),
            db.Column('SellPrice', db.Float, nullable = True)
            )

def init(CurrencyPair):
    engine = db.create_engine('sqlite:///./Data/CurrencyPair_{}/data.sqlite'.format(CurrencyPair))
    conn = engine.connect()
    metadata.create_all(engine) 
    deletemoney(CurrencyPair)
    deleteTrades(CurrencyPair)

def checkmoney(CurrencyPair):
    engine = db.create_engine('sqlite:///./Data/CurrencyPair_{}/data.sqlite'.format(CurrencyPair))
    conn = engine.connect()
    output = conn.execute("SELECT newMoney FROM Money")
    result = output.fetchall()
    if len(result) > 0:
        return float(result[-1][0])
    return 0.0

def deletemoney(CurrencyPair):
    engine = db.create_engine('sqlite:///./Data/CurrencyPair_{}/data.sqlite'.format(CurrencyPair))
    conn = engine.connect()
    conn.execute("DELETE FROM Money")

def insertmoney(CurrencyPair,money):
    engine = db.create_engine('sqlite:///./Data/CurrencyPair_{}/data.sqlite'.format(CurrencyPair))
    conn = engine.connect()
    oldmoney = checkmoney(CurrencyPair)
    query = db.insert(Money).values(oldMoney=oldmoney,newMoney=money)
    conn.execute(query)

def deleteTrades(CurrencyPair):
    engine = db.create_engine('sqlite:///./Data/CurrencyPair_{}/data.sqlite'.format(CurrencyPair))
    conn = engine.connect()
    conn.execute("DELETE FROM Trades")

def insertTrade(CurrencyPair, price, Qty, Kind):
    engine = db.create_engine('sqlite:///./Data/CurrencyPair_{}/data.sqlite'.format(CurrencyPair))
    conn = engine.connect()
    query = db.insert(Trades).values(price=price,Qty=Qty, Status='FILLED', Kind=Kind)
    conn.execute(query)
    
def checkopenTrade(CurrencyPair):
    engine = db.create_engine('sqlite:///./Data/CurrencyPair_{}/data.sqlite'.format(CurrencyPair))
    conn = engine.connect()
    output = conn.execute("SELECT * FROM Trades WHERE Status = 'FILLED'")
    result = output.fetchall()
    if len(result) == 0:
        return False
    return result

def updateTradeTrailing(CurrencyPair, Id, trailing):
    engine = db.create_engine('sqlite:///./Data/CurrencyPair_{}/data.sqlite'.format(CurrencyPair))
    conn = engine.connect()
    query = db.update(Trades).where(Trades.c.Id==Id).values(Trailing=trailing)
    conn.execute(query)

def updateTradeSell(CurrencyPair, Id, SellPrice):
    engine = db.create_engine('sqlite:///./Data/CurrencyPair_{}/data.sqlite'.format(CurrencyPair))
    conn = engine.connect()
    query = db.update(Trades).where(Trades.c.Id==Id).values(SellPrice=SellPrice,Status='SELL')
    conn.execute(query)
