from requests.auth import HTTPBasicAuth
import requests
import helper.mysql
import logging
import helper.config

conf = helper.config.initconfig()

def ping(url):
    try:
        url = "http://" + url + "/api/v1/ping"
        resp = requests.get(url=url)
        data = resp.json()
        if data['status'] == 'pong':
            return True
        return False
    except Exception as e:
        logging.error("Fehler in freqtrade.py ping(): " + str(e))
        print("Fehler in freqtrade.py ping(): " + str(e))
        return False

def daily(url,user,pw):
    try:
        url = "http://" + url + "/api/v1/daily"
        resp = requests.get(url=url, auth=HTTPBasicAuth(user, pw))
        data = resp.json()
        return data
    except Exception as e:
        logging.error("Fehler in freqtrade.py daily(): " + str(e))
        print("Fehler in freqtrade.py daily(): " + str(e))

def profit(url,user,pw):
    try:
        url = "http://" + url + "/api/v1/profit"
        resp = requests.get(url=url, auth=HTTPBasicAuth(user, pw))
        data = resp.json()
        return data
    except Exception as e:
        logging.error("Fehler in freqtrade.py daily(): " + str(e))
        print("Fehler in freqtrade.py daily(): " + str(e))

def balance(url,user,pw):
    try:
        url = "http://" + url + "/api/v1/balance"
        resp = requests.get(url=url, auth=HTTPBasicAuth(user, pw))
        data = resp.json()
        return data
    except Exception as e:
        logging.error("Fehler in freqtrade.py daily(): " + str(e))
        print("Fehler in freqtrade.py daily(): " + str(e))

def stats(url,user,pw):
    try:
        url = "http://" + url + "/api/v1/stats"
        resp = requests.get(url=url, auth=HTTPBasicAuth(user, pw))
        data = resp.json()
        return data
    except Exception as e:
        logging.error("Fehler in freqtrade.py daily(): " + str(e))
        print("Fehler in freqtrade.py daily(): " + str(e))

def performance(url,user,pw):
    try:
        url = "http://" + url + "/api/v1/performance"
        resp = requests.get(url=url, auth=HTTPBasicAuth(user, pw))
        data = resp.json()
        return data
    except Exception as e:
        logging.error("Fehler in freqtrade.py daily(): " + str(e))
        print("Fehler in freqtrade.py daily(): " + str(e))