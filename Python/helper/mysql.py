from datetime import datetime
import logging
import time

import helper.config
import mysql.connector

conf = helper.config.initconfig()


def init():
    mydb = mysql.connector.connect(
        host = conf['MYSQL_HOST'],
        user = conf['MYSQL_USER'],
        password = conf['MYSQL_PASS'],
    )
    mycursor = mydb.cursor()

    mycursor.execute("CREATE DATABASE IF NOT EXISTS Mining")
    
    mycursor.close()

    mydb = mysql.connector.connect(
    host = conf['MYSQL_HOST'],
    user = conf['MYSQL_USER'],
    password = conf['MYSQL_PASS'],
    database = 'Mining'
    )
    mycursor = mydb.cursor()

    # Nicehash
    mycursor.execute('''CREATE TABLE IF NOT EXISTS nicehash_devices
                    (device integer, temperature real, loads real, powerUsage real, timestamp integer)''')

    # Nicehash
    mycursor.execute('''CREATE TABLE IF NOT EXISTS nicehash_profitability
                    (profitability real, timestamp integer)''')

    # ETH Miners
    mycursor.execute('''CREATE TABLE IF NOT EXISTS EthminersAPI
                    (reward24h integer, currentHashrate integer, currentLuck real, hashrate integer, timestamp integer)''')
    
    # XMR Miners
    mycursor.execute('''CREATE TABLE IF NOT EXISTS XmrminersAPI
                    (reward24h integer, currentHashrate integer, hashrate integer, timestamp integer)''')

    # Psutil
    mycursor.execute('''CREATE TABLE IF NOT EXISTS psutil
                    (name text, 
                    cpu1 real, cpu2 real, cpu3 real, cpu4 real, cpu5 real, cpu6 real, cpu7 real, cpu8 real, 
                    cpu_freq real, ram real, disk real, swap_memory real,
                    disktemp real, coretemp1 real, coretemp2 real, coretemp3 real, gputemp1 real, gputemp2 real,
                    fan1 real, fan2 real, timestamp integer)''')
    
    # Groove
    mycursor.execute('''CREATE TABLE IF NOT EXISTS groove
                    (name text,
                    case_temp real, case_humi real, timestamp integer)''')
    
    # Arduino
    mycursor.execute('''CREATE TABLE IF NOT EXISTS arduino
                    (name text,
                    tank_temp real, line1_temp real, line2_temp real, durchfluss1 real, durchfluss2 real, timestamp integer)''')
    
    mycursor.close()

def insertNicehashDevices(data):
    try:
        mydb = mysql.connector.connect(
        host = conf['MYSQL_HOST'],
        user = conf['MYSQL_USER'],
        password = conf['MYSQL_PASS'],
        database = 'Mining'
        )
        mycursor = mydb.cursor()
        execute = """INSERT INTO nicehash_devices (device, temperature, loads, powerUsage, timestamp) VALUES (%s,%s,%s,%s,%s)"""
        query = data['device'], data['temperature'], data['load'], data['powerUsage'], time.time()
        mycursor.execute(execute, query)
        mydb.commit()
        mycursor.close()
    except Exception as e:
        logging.error("Fehler beim schreiben von Nicehash Devices in SQL: " + str(e))
        print("Fehler beim schreiben von Nicehash Devices in SQL: " + str(e))

def insertNicehashProfitability(data):
    try:
        mydb = mysql.connector.connect(
        host = conf['MYSQL_HOST'],
        user = conf['MYSQL_USER'],
        password = conf['MYSQL_PASS'],
        database = 'Mining'
        )
        mycursor = mydb.cursor()
        execute = """INSERT INTO nicehash_profitability (profitability, timestamp) VALUES (%s,%s)"""
        query = data['profitability'], time.time()
        mycursor.execute(execute, query)
        mydb.commit()
        mycursor.close()
    except Exception as e:
        logging.error("Fehler beim schreiben von Nicehash Profitability in SQL: " + str(e))
        print("Fehler beim schreiben von Nicehash Profitability in SQL: " + str(e))


# SELECT AVG(val) FROM ts_number WHERE id = 10 ORDER BY ts DESC LIMIT 10; 

# Request
def requestsolarpower():
    #try:
        mydb = mysql.connector.connect(
        host = conf['MYSQL_HOST'],
        user = conf['MYSQL_USER'],
        password = conf['MYSQL_PASS'],
        database = 'iobrokerwinzerhausen'
        )
        mycursor = mydb.cursor()
        mycursor.execute("SELECT val FROM ts_number WHERE id = 10 ORDER BY ts DESC LIMIT 10")
        myresult = mycursor.fetchall()
        mycursor.close()

        return myresult
    # except Exception as e:
    #     logging.error("Fehler beim lesen von solarpower Daten aus SQL: " + str(e))
    #     print("Fehler beim lesen von solarpower Daten aus SQL: " + str(e))



# 2Miners API ETH
# Insert
def insert2minersEthAPI(data):
    try:
        mydb = mysql.connector.connect(
        host = conf['MYSQL_HOST'],
        user = conf['MYSQL_USER'],
        password = conf['MYSQL_PASS'],
        database = 'Mining'
        )
        mycursor = mydb.cursor()
        execute = """INSERT INTO EthminersAPI (reward24h, currentHashrate, currentLuck, hashrate, timestamp) VALUES (%s,%s,%s,%s,%s)"""
        query = data['24hreward'], data['currentHashrate'], data['currentLuck'], data['hashrate'], int(time.time())
        mycursor.execute(execute, query)
        mydb.commit()
        mycursor.close()
    except Exception as e:
        logging.error("Fehler beim schreiben von ETH 2Miners Daten in SQL: " + str(e))
        print("Fehler beim schreiben von ETH 2Miners Daten in SQL: " + str(e))    

# Request
def request2minersEthAPI():
    try:
        mydb = mysql.connector.connect(
        host = conf['MYSQL_HOST'],
        user = conf['MYSQL_USER'],
        password = conf['MYSQL_PASS'],
        database = 'Mining'
        )
        mycursor = mydb.cursor()
        mycursor.execute("SELECT * FROM `EthminersAPI` ORDER BY timestamp DESC LIMIT 1")
        myresult = mycursor.fetchone()
        mycursor.close()

        # JSON
        resultjson = {}
        resultjson['reward24h'] = myresult[0]
        resultjson['currentHashrate'] = myresult[1]
        resultjson['currentLuck'] = myresult[2]
        resultjson['hashrate'] = myresult[3]
        resultjson['timestamp'] = myresult[4]
        return resultjson
    except Exception as e:
        logging.error("Fehler beim lesen von ETH 2Miners Daten aus SQL: " + str(e))
        print("Fehler beim lesen von ETH 2Miners Daten aus SQL: " + str(e))

# 2Miners API XMR
# Insert
def insert2minersXmrAPI(data):
    try:
        mydb = mysql.connector.connect(
        host = conf['MYSQL_HOST'],
        user = conf['MYSQL_USER'],
        password = conf['MYSQL_PASS'],
        database = 'Mining'
        )
        mycursor = mydb.cursor()
        execute = """INSERT INTO XmrminersAPI (reward24h, currentHashrate, hashrate, timestamp) VALUES (%s,%s,%s,%s)"""
        query = data['24hreward'], data['currentHashrate'], data['hashrate'], int(time.time())
        mycursor.execute(execute, query)
        mydb.commit()
        mycursor.close()
    except Exception as e:
        logging.error("Fehler beim schreiben von XMR 2Miners Daten in SQL: " + str(e))
        print("Fehler beim schreiben von XMR 2Miners Daten in SQL: " + str(e))    

# Request
def request2minersXmrAPI():
    try:
        mydb = mysql.connector.connect(
        host = conf['MYSQL_HOST'],
        user = conf['MYSQL_USER'],
        password = conf['MYSQL_PASS'],
        database = 'Mining'
        )
        mycursor = mydb.cursor()
        mycursor.execute("SELECT * FROM `XmrminersAPI` ORDER BY timestamp DESC LIMIT 1")
        myresult = mycursor.fetchone()
        mycursor.close()

        # JSON
        resultjson = {}
        resultjson['reward24h'] = myresult[0]
        resultjson['currentHashrate'] = myresult[1]
        resultjson['hashrate'] = myresult[2]
        resultjson['timestamp'] = myresult[3]
        return resultjson
    except Exception as e:
        logging.error("Fehler beim lesen von XMR 2Miners Daten aus SQL: " + str(e))
        print("Fehler beim lesen von XMR 2Miners Daten aus SQL: " + str(e))


def insertpsutil(data):
    try:
        mydb = mysql.connector.connect(
        host = conf['MYSQL_HOST'],
        user = conf['MYSQL_USER'],
        password = conf['MYSQL_PASS'],
        database = 'Mining'
        )
        # Fan Speed normieren
        if data['fans'] == {}:
            data['fan1'] = 0
            data['fan2'] = 0

        if data['fans']['amdgpu'][0][1] == '':
            data['fan1'] = 0
        else:
            data['fan1'] = data['fans']['amdgpu'][0][1]

        if data['fans']['amdgpu'][0][1] == '':
            data['fan2'] = 0
        else:
            data['fan2'] = data['fans']['amdgpu'][0][1]
        
        # Disk Temp normieren
        if 'nvme' in data['temperatures']:
            data['disktemp'] = data['temperatures']['nvme'][0][1]
        else:
            data['disktemp'] = 0
        
        # CPU Temp normieren
        if 'coretemp' in data['temperatures']:
            data['coretemp1'] = data['temperatures']['coretemp'][0][1]
            data['coretemp2'] = 0
            data['coretemp3'] = 0
        elif 'k10temp' in data['temperatures']:
            data['coretemp1'] = data['temperatures']['k10temp'][0][1]
            data['coretemp2'] = data['temperatures']['k10temp'][1][1]
            data['coretemp3'] = 0
        else:
            data['coretemp1'] = 0
            data['coretemp2'] = 0
            data['coretemp3'] = 0
        
        # GPU Temp normieren
        if 'amdgpu' in data['temperatures']:
            data['gputemp1'] = data['temperatures']['amdgpu'][0][1]
            data['gputemp2'] = data['temperatures']['amdgpu'][1][1]
        else:
            data['gputemp1'] = 0
        
        mycursor = mydb.cursor()
        execute = """INSERT INTO psutil (name, cpu1, cpu2, cpu3, cpu4, cpu5, cpu6, cpu7, cpu8, cpu_freq, ram, disk, swap_memory, disktemp, coretemp1, coretemp2, coretemp3, gputemp1, gputemp2, fan1, fan2, timestamp) VALUES 
                    (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,
                     %s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
        query = data['name'],\
                data['cpu'][0], data['cpu'][1], data['cpu'][2], data['cpu'][3], \
                data['cpu'][4], data['cpu'][5], data['cpu'][6], data['cpu'][7],  \
                data['cpu_freq'][0], data['ram'], data['disk'], data['swap_memory'], \
                data['disktemp'], data['coretemp1'], data['coretemp2'], data['coretemp3'], data['gputemp1'], data['gputemp2'],\
                data['fan1'], data['fan2'], time.time()
                # TODO
                # only for 8 cores

        mycursor.execute(execute, query)
        mydb.commit()
        mycursor.close()
    except Exception as e:
        logging.error("Fehler beim schreiben von GPU in SQL: " + str(e))
        print("Fehler beim schreiben von GPU in SQL: " + str(e))

def insertgroove(data):
    try:
        mydb = mysql.connector.connect(
        host = conf['MYSQL_HOST'],
        user = conf['MYSQL_USER'],
        password = conf['MYSQL_PASS'],
        database = 'Mining'
        )
        mycursor = mydb.cursor()
        execute = """INSERT INTO groove (name, case_temp, case_humi, timestamp) VALUES (%s,%s,%s,%s)"""
        query = data['name'], data['case_temp'], data['case_humi'], time.time()
        mycursor.execute(execute, query)
        mydb.commit()
        mycursor.close()
    except Exception as e:
        logging.error("Fehler beim schreiben von Grove in SQL: " + str(e))
        print("Fehler beim schreiben von Grove in SQL: " + str(e))

def insertarduino(data):
    try:
        mydb = mysql.connector.connect(
        host = conf['MYSQL_HOST'],
        user = conf['MYSQL_USER'],
        password = conf['MYSQL_PASS'],
        database = 'Mining'
        )
        mycursor = mydb.cursor()
        execute = """INSERT INTO arduino (name, tank_temp, line1_temp, line2_temp, durchfluss1, durchfluss2, timestamp) VALUES (%s,%s,%s,%s,%s,%s,%s)"""
        query = data['name'], data['tank_temp'], data['line1_temp'], data['line2_temp'], data['durchfluss1'], data['durchfluss2'], time.time()
        mycursor.execute(execute, query)
        mydb.commit()
        mycursor.close()
    except Exception as e:
        logging.error("Fehler beim schreiben von Arduino in SQL: " + str(e))
        print("Fehler beim schreiben von Arduino in SQL: " + str(e))
