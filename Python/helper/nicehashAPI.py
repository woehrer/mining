import requests
import datetime
import time
import json
import hmac

import helper.config

import uuid, hmac, hashlib, requests, json, base64

conf = helper.config.initconfig()

# https://www.nicehash.com/docs/rest/get-main-api-v2-mining-algo-stats

# GET https://api2.nicehash.com/main/api/v2/mining/rigs


def request(pth):
    urlroot = 'https://api2.nicehash.com'
    xtime = str(json.loads(requests.get('https://api2.nicehash.com/api/v2/time').text)['serverTime'])
    xnonce = str(uuid.uuid4())
    xorid = conf['nicehashorgid']
    apikey = conf['nicehashtoken']
    mthd = 'GET'
    #pth = '/main/api/v2/mining/rigs'
    pth = pth
    qry = 'size=100&page=0'
    scrt = conf['nicehashsecret']
    inpt = '{}\00{}\00{}\00\00{}\00\00{}\00{}\00{}'.format(apikey, xtime, xnonce, xorid, mthd, pth, qry)
    sig = hmac.new(scrt.encode(), inpt.encode(), hashlib.sha256).hexdigest()
    xauth = '{}:{}'.format(apikey, sig)
    r = requests.get('{}{}?{}'.format(urlroot, pth, qry), headers={'X-Time': xtime, 'X-Nonce': xnonce, 'X-Organization-Id': xorid, 'X-Request-Id': xnonce, 'X-Auth': xauth})
    data = json.loads(r.text)
    return data

def request_post(pth,body):
    urlroot = 'https://api2.nicehash.com'
    xtime = str(json.loads(requests.get('https://api2.nicehash.com/api/v2/time').text)['serverTime'])
    xnonce = str(uuid.uuid4())
    xorid = conf['nicehashorgid']
    apikey = conf['nicehashtoken']
    mthd = 'POST'
    #pth = '/main/api/v2/mining/rigs'
    pth = pth
    qry = ''
    scrt = conf['nicehashsecret']
    serialized_body = json.dumps(body)
    content = serialized_body.encode("utf-8")
    body_json = json.dumps(body)
    inpt = '{}\00{}\00{}\00\00{}\00\00{}\00{}\00{}\00{}'.format(apikey, xtime, xnonce, xorid, mthd, pth, qry, body_json)
    sig = hmac.new(scrt.encode(), inpt.encode(), hashlib.sha256).hexdigest()
    xauth = '{}:{}'.format(apikey, sig)
    r = requests.post('{}{}?{}'.format(urlroot, pth, qry), headers={'X-Time': xtime, 'X-Nonce': xnonce, 'X-Organization-Id': xorid, 'X-Request-Id': xnonce, 'X-Auth': xauth, 'Content-Type': 'application/json;charset=UTF-8',}, data=body_json)
    data = json.loads(r.text)
    return data