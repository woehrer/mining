import requests
import helper.mysql
import logging
import helper.config

conf = helper.config.initconfig()

def ethaccount():
    try:
        url = "https://eth.2miners.com/api/"
        url = url + "accounts/"
        url = url + str(conf['eth_walletid'])
        resp = requests.get(url=url)
        data = resp.json()
        helper.mysql.insert2minersEthAPI(data)
    except Exception as e:
        logging.error("Fehler in minersAPI.py account(): " + str(e))
        print("Fehler in minersAPI.py account(): " + str(e))

def xmraccount():
    try:
        url = "https://xmr.2miners.com/api/"
        url = url + "accounts/"
        url = url + str(conf['xmr_walletid'])
        resp = requests.get(url=url)
        data = resp.json()
        helper.mysql.insert2minersXmrAPI(data)
    except Exception as e:
        logging.error("Fehler in minersAPI.py account(): " + str(e))
        print("Fehler in minersAPI.py account(): " + str(e))
