import sqlite3
import time

def init():
    con = sqlite3.connect('sqlite.db')
    cur = con.cursor()

    # Create table
    cur.execute('''CREATE TABLE IF NOT EXISTS Buys (
                symbol TEXT,
                orderId INTEGER,
                orderListId INTEGER,
                clientOrderId TEXT,
                transactTime INTEGER,
                price REAL,
                origQty REAL,
                executedQty REAL,
                cummulativeQuoteQty REAL,
                status TEXT,
                timeInForce TEXT,
                type TEXT,
                side TEXT,
                sellID INTEGER,
                trailingProfit INTEGER,
                trade_id INTEGER PRIMARY KEY,
                kind TEXT
                )''')

    cur.execute('''CREATE TABLE IF NOT EXISTS Sells (
                symbol TEXT,
                orderId INTEGER,
                orderListId INTEGER,
                clientOrderId TEXT,
                transactTime INTEGER,
                price REAL,
                origQty REAL,
                executedQty REAL,
                cummulativeQuoteQty REAL,
                status TEXT,
                timeInForce TEXT,
                type TEXT,
                side TEXT,
                buyID INTEGER,
                trade_id INTEGER PRIMARY KEY
                )''')


    cur.execute('''CREATE TABLE IF NOT EXISTS Profits (
                symbol TEXT,
                buyId INTEGER,
                sellId INTEGER,
                profit REAL,
                profit_USDT REAL,
                profit_id INTEGER PRIMARY KEY
                )''')


def insertBuyTrade(data,kind):
    con = sqlite3.connect('sqlite.db')
    cur = con.cursor()
    
    execute = 'INSERT INTO Buys (symbol, orderId, orderListId, clientOrderId, transactTime, price, origQty, executedQty, cummulativeQuoteQty, status, timeInForce, type, side, sellId, kind) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
    query = (data['symbol'],\
            data['orderId'],\
            data['orderListId'],\
            data['clientOrderId'],\
            data['transactTime'],\
            data['price'],\
            data['origQty'],\
            data['executedQty'],\
            data['cummulativeQuoteQty'],\
            data['status'],\
            data['timeInForce'],\
            data['type'],\
            data['side'],\
            "",\
            kind)
    cur.execute(execute,query)
    con.commit()
    con.close()

def insertSellTrade(data, BuyId):
    con = sqlite3.connect('sqlite.db')
    cur = con.cursor()
    
    execute = 'INSERT INTO Sells (symbol, orderId, orderListId, clientOrderId, transactTime, price, origQty, executedQty, cummulativeQuoteQty, status, timeInForce, type, side, buyId) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
    query = (data['symbol'],\
            data['orderId'],\
            data['orderListId'],\
            data['clientOrderId'],\
            data['transactTime'],\
            data['price'],\
            data['origQty'],\
            data['executedQty'],\
            data['cummulativeQuoteQty'],\
            data['status'],\
            data['timeInForce'],\
            data['type'],\
            data['side'],\
            BuyId)
    cur.execute(execute,query)
    con.commit()
    con.close()


def insertProfit(symbol, BuyId, SellId, profit, profit_USDT):
    con = sqlite3.connect('sqlite.db')
    cur = con.cursor()
    
    execute = 'INSERT INTO Profits (symbol, buyId, sellId, profit, profit_USDT) VALUES (?,?,?,?,?)'
    query = (symbol, BuyId, SellId, profit, profit_USDT)
    cur.execute(execute,query)
    con.commit()
    con.close()


def getSearchNewBuys():
    con = sqlite3.connect('sqlite.db')
    cur = con.cursor()
    execute = "SELECT * FROM Buys WHERE status = 'NEW'"
    cur.execute(execute)
    result = cur.fetchall()
    con.close()
    return result

def getSearchFilledBuys():
    con = sqlite3.connect('sqlite.db')
    cur = con.cursor()
    execute = "SELECT * FROM Buys WHERE status = 'FILLED' and sellID = ''"
    cur.execute(execute)
    result = cur.fetchall()
    con.close()
    return result

def getSearchFilledBuysall():
    con = sqlite3.connect('sqlite.db')
    cur = con.cursor()
    execute = "SELECT * FROM Buys WHERE status = 'FILLED' and sellID = '' ORDER BY symbol ASC"
    cur.execute(execute)
    result = cur.fetchall()
    con.close()
    return result

def getSearchNewSells():
    con = sqlite3.connect('sqlite.db')
    cur = con.cursor()
    execute = "SELECT * FROM Sells WHERE status = 'NEW'"
    cur.execute(execute)
    result = cur.fetchall()
    con.close()
    return result

def getSearchFilledSells():
    con = sqlite3.connect('sqlite.db')
    cur = con.cursor()
    execute = "SELECT * FROM Sells WHERE status = 'FILLED'"
    cur.execute(execute)
    result = cur.fetchall()
    con.close()
    return result

def getSearchorderId(Id): # TODO change to new ID
    con = sqlite3.connect('sqlite.db')
    cur = con.cursor()
    execute = "SELECT * FROM Buys WHERE orderId = " + str(Id)
    cur.execute(execute)
    result = cur.fetchone()
    con.close()
    return result

def getSearchorderId2(Id):
    con = sqlite3.connect('sqlite.db')
    cur = con.cursor()
    execute = "SELECT * FROM Buys WHERE trade_id = " + str(Id)
    cur.execute(execute)
    result = cur.fetchone()
    con.close()
    return result

def updateBuyTrade(data):
    con = sqlite3.connect('sqlite.db')
    cur = con.cursor()
    execute = "UPDATE Buys SET symbol=?, orderId=?, orderListId=?, clientOrderId=?, transactTime=?, price=?, origQty=?, executedQty=?, cummulativeQuoteQty=?, status=?, timeInForce=?, type=?, side=? WHERE status = 'NEW' and orderId = ?"
    query =(    data['symbol'],\
                data['orderId'],\
                data['orderListId'],\
                data['clientOrderId'],\
                data['updateTime'],\
                data['price'],\
                data['origQty'],\
                data['executedQty'],\
                data['cummulativeQuoteQty'],\
                data['status'],\
                data['timeInForce'],\
                data['type'],\
                data['side'],\
                data['orderId'])
    cur.execute(execute,query)
    con.commit()
    con.close()

def updateTrailingTrade(id, trailingProfit):
    con = sqlite3.connect('sqlite.db')
    cur = con.cursor()
    execute = "UPDATE Buys SET trailingProfit = ? WHERE orderId = ?"
    query = (trailingProfit, id)
    cur.execute(execute,query)
    con.commit()
    con.close()

def updateTrailingTradetoNULL(id):
    con = sqlite3.connect('sqlite.db')
    cur = con.cursor()
    execute = "UPDATE Buys SET trailingProfit = NULL WHERE orderId = '" + str(id) + "'"
    cur.execute(execute)
    con.commit()
    con.close()

def updateSellTrade(data):
    con = sqlite3.connect('sqlite.db')
    cur = con.cursor()
    execute = "UPDATE Sells SET symbol=?, orderId=?, orderListId=?, clientOrderId=?, transactTime=?, price=?, origQty=?, executedQty=?, cummulativeQuoteQty=?, status=?, timeInForce=?, type=?, side=? WHERE status = 'NEW' and orderId = ?"
    query =(    data['symbol'],\
                data['orderId'],\
                data['orderListId'],\
                data['clientOrderId'],\
                data['updateTime'],\
                data['price'],\
                data['origQty'],\
                data['executedQty'],\
                data['cummulativeQuoteQty'],\
                data['status'],\
                data['timeInForce'],\
                data['type'],\
                data['side'],\
                data['orderId'])
    cur.execute(execute,query)
    con.commit()
    con.close()

def updateBuywithSellid(orderId, sellId):
    con = sqlite3.connect('sqlite.db')
    cur = con.cursor()
    execute = "UPDATE Buys SET sellId=? WHERE orderId = ?"
    query =(    sellId,\
                orderId)
    cur.execute(execute,query)
    con.commit()
    con.close()

def deleteBuyTrade(data):
    con = sqlite3.connect('sqlite.db')
    cur = con.cursor()
    execute = "DELETE FROM Buys WHERE status = 'NEW' and orderId = " + str(data['orderId'])
    cur.execute(execute)
    con.commit()
    con.close()

def deleteSellTrade(data):
    con = sqlite3.connect('sqlite.db')
    cur = con.cursor()
    execute = "DELETE FROM Sells WHERE status = 'NEW' and orderId = " + str(data['orderId'])
    cur.execute(execute)
    con.commit()
    con.close()

def gettradeprotection():
    con = sqlite3.connect('sqlite.db')
    cur = con.cursor()
    protectiontime = (int(time.time()) - 3300)*1000
    execute = "SELECT * FROM Buys a, Sells b WHERE a.transactTime > " + str(protectiontime) + " or b.transactTime > " + str(protectiontime)
    cur.execute(execute)
    result = cur.fetchall()
    if len(result) > 0:
        return True
    return False
