import strategies.main
import pandas as pd
import time
import helper.sqlmanager
import helper.trade

def backtest(CurrencyPair):
    df = pd.read_csv('./Data/CurrencyPair_{}/Sorted.csv'.format(CurrencyPair))

    money = 100

    set_size = 11

    helper.sqlmanager.init(CurrencyPair)
    helper.sqlmanager.insertmoney(CurrencyPair,money)

    index = len(df.index)
  
    zaehler = 0

    while zaehler < index:
        if zaehler > 500:
            new_df = df[zaehler-500:zaehler]
            # BUY
            if strategies.main.handler(new_df):
                money = helper.sqlmanager.checkmoney(CurrencyPair)
                for kind in helper.trade.kinds:
                    if  money > set_size*1.01:
                        # TODO insert Trade in Backtest DB
                        print("BUY")
                        helper.sqlmanager.insertmoney(CurrencyPair, money-set_size)
                        Qty = set_size / float(new_df['close'].iloc[-1])
                        helper.sqlmanager.insertTrade(CurrencyPair, new_df['close'].iloc[-1], Qty, kind)


            # SELL
            Trades = helper.sqlmanager.checkopenTrade(CurrencyPair)
            if Trades:
                for trade in Trades:
                    if trade['Kind'] == "Fast":
                        trailingvalue = 1.0
                        trailingoffset = 2.0
                    elif trade['Kind'] == "Middle":
                        trailingvalue = 2.5
                        trailingoffset = 5.0
                    else:
                        trailingvalue = 5.0
                        trailingoffset = 10.0
                    profit = float(((new_df['close'].iloc[-1]/trade['price'])*100)-100)
                    print(profit)

                    if profit > trailingoffset:
                        if not trade['Trailing']:
                            trailing = profit - trailingvalue
                            helper.sqlmanager.updateTradeTrailing(CurrencyPair, trade['Id'], trailing)
                        else: 
                            if profit  > trade['Trailing'] + trailingvalue:
                                trailing = profit - trailingvalue
                                helper.sqlmanager.updateTradeTrailing(CurrencyPair, trade['Id'], trailing)
                    if not(not trade['Trailing']):
                        if profit > 0.0:
                            if profit < trade['Trailing']:
                                print("SELL!!!!!!!!!!!!!!!")
                                helper.sqlmanager.updateTradeSell(CurrencyPair, trade['Id'], new_df['close'].iloc[-1])
                                money = helper.sqlmanager.checkmoney(CurrencyPair)
                                win = float(new_df['close'].iloc[-1])*float(trade['Qty'])
                                print("Win: ",win)
                                helper.sqlmanager.insertmoney(CurrencyPair, money + win)
                

        zaehler += 1
