import os
import psutil
import requests
import helper.config
import logging

conf = helper.config.initconfig()


def check():
    data = {}

    # CPU
    data['cpu'] = psutil.cpu_percent(interval=1, percpu=True)
    print("CPU: " + str(data['cpu']) + "%")

    data['cpu_count'] = psutil.cpu_count()
    print("CPU Count: " + str(data['cpu_count']))

    data['cpu_percent'] = psutil.cpu_percent(interval=1, percpu=True)
    print("CPU Percent: " + str(data['cpu_percent']) + "%")

    data['cpu_freq'] = psutil.cpu_freq()
    print("CPU Freq: " + str(data['cpu_freq']))

    # RAM
    data['ram'] = psutil.virtual_memory().percent
    print("RAM: " + str(data['ram']) + "%")

    data['disk'] = psutil.disk_usage('/').percent
    print("Disk: " + str(data['disk']) + "%")

    data['swap_memory'] = psutil.swap_memory().percent
    print("Swap Memory: " + str(data['swap_memory']) + "%")

    # GPU
    data['temperatures'] = psutil.sensors_temperatures()
    print("Temperatures: " + str(data['temperatures']) + "°C")

    data['fans'] = psutil.sensors_fans()
    print("Fans: " + str(data['fans']) + "rpm")

    data['power'] = psutil.sensors_battery()
    print("Power: " + str(data['power']))

    request(data)

def request(data):
    try:
        res = requests.post('http://' + conf['API_HOST'] + ':5000/api/psutil/' + os.uname().nodename, json=data, timeout=5)
        if res.ok:
            print(res.json())
    except Exception as e:
        logging.error("Fehler beim schreiben von psutil Daten in API: " + str(e))
        print("Fehler beim schreiben von psutil Daten in API: " + str(e))