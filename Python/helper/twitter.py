import logging
import tweepy

import helper.config

conf = helper.config.initconfig()


client = tweepy.Client(consumer_key=conf['Twitter_Consumer_Key'],
                        consumer_secret=conf['Twitter_Consumer_Secret'],
                        access_token=conf['Twitter_Access_Token'],
                        access_token_secret=conf['Twitter_Access_Token_Secret'])


def tweet(text):
    try:
        text = text + "\n #freqtrade #tradingbot #Trading #Bitcoin #Ethereum #Tradingsignals"
        client.create_tweet(text = text)
    except Exception as e:
        logging.error("Fehler beim Tweet: " + str(e))
        print("Fehler beim Tweet: " + str(e))

def follow_followers():
    me = client.get_me()
    followers = client.get_users_followers(me.data.id,user_auth=True)
    print(followers)
    for follower in followers.data:
        client.follow_user(follower.id)