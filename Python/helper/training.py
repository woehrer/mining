import csv
import json
import multiprocessing
import time
import datetime

import numpy as np
import pandas as pd
import talib.abstract as ta
from joblib import dump, load

import helper.binance
import helper.config
import helper.trade

conf = helper.config.initconfig()

def compute_sma(df, window, colname):
    '''Computes Simple Moving Average column on a dataframe'''
    df[colname] = df['close'].rolling(window=window, center=False).mean()
    return(df)

def compute_rsi(df, window, colname):
    '''Computes RSI column for a dataframe. http://stackoverflow.com/a/32346692/3389859'''
    series = df['close']
    delta = series.diff().dropna()
    u = delta * 0
    d = u.copy()
    u[delta > 0] = delta[delta > 0]
    d[delta < 0] = -delta[delta < 0]
    # first value is sum of avg gains
    u[u.index[window - 1]] = np.mean(u[:window])
    u = u.drop(u.index[:(window - 1)])
    # first value is sum of avg losses
    d[d.index[window - 1]] = np.mean(d[:window])
    d = d.drop(d.index[:(window - 1)])
    rs = u.ewm(com=window - 1,ignore_na=False,
               min_periods=0,adjust=False).mean() / d.ewm(com=window - 1, ignore_na=False,
                                            min_periods=0,adjust=False).mean()
    df[colname] = 100 - 100 / (1 + rs)
    df[colname].fillna(df[colname].mean(), inplace=True)
    return(df)

def concat(CurrencyPairList):
    list = []

    print("Concat")

    for CurrencyPair in CurrencyPairList:
        list.append('./KI/Data/CurrencyPair_{}/Sorted.csv'.format(CurrencyPair))

    df = pd.concat(
    map(pd.read_csv, list), ignore_index=True)

    print(df)

    return df

def sort_data(CurrencyPair, Mode):
    if Mode == 0:
        df = pd.read_csv('./KI/Data/CurrencyPair_{}/Data.csv'.format(CurrencyPair))
    else:
        df = pd.read_csv('./KI/Actual/CurrencyPair_{}/Data.csv'.format(CurrencyPair))

    # Convert the UNIX time into the date
    df["Date"] = pd.to_datetime(df["Open time"], unit='ms')

    # Sort data by date
    df = df.sort_values(by = 'Date')

    # Use only the important values
    # df = df[['close', 'Date', 'high', 'low', 'open', 'volume']]

    #df["Date"] = pd.to_datetime(df["Date"])
    df['close'] = pd.to_numeric(df['close'])
    df['open'] = pd.to_numeric(df['open'])
    df['Bodysize'] = df['close'] - df['open']
    df['high'] = pd.to_numeric(df['high'])
    df['low'] = pd.to_numeric(df['low'])
    df['Shadowsize'] = df['high'] - df['low']
    #TODO evtl Reihe erhöhen und auf auswirkungen schauen
    for window in [3, 8, 21, 55, 144, 377]: # several Fibonacci numbers
        # SMA
        df = compute_sma(df, window, colname = 'sma_{}'.format(window))
        # RSI
        df = compute_rsi(df, window, colname = 'rsi_{}'.format(window))
        # Values
        df["Min_{}".format(window)] = df["low"].rolling(window).min()
        df["Max_{}".format(window)] = df["high"].rolling(window).max()
        df["volume_{}".format(window)] = df["volume"].rolling(window).mean()
        df['PercentChange_{}'.format(window)] = df['close'].pct_change(periods = window)
        df['RelativeSize_sma_{}'.format(window)] = df['close'] / df['sma_{}'.format(window)]
        df['Diff_{}'.format(window)] = df['close'].diff(window)

    # Add modulo 10, 100, 1000, 500, 50
    df["Modulo_10"] = df["close"].copy() % 10
    df["Modulo_100"] = df["close"].copy() % 100
    df["Modulo_1000"] = df["close"].copy() % 1000
    df["Modulo_500"] = df["close"].copy() % 500
    df["Modulo_50"] = df["close"].copy() % 50

    # Add weekday and day of the month
    df["WeekDay"] = df["Date"].dt.weekday
    df["Day"] = df["Date"].dt.day


    # EMA - Exponential Moving Average
    df['ema3'] = ta.EMA(df, timeperiod=3)
    df['ema5'] = ta.EMA(df, timeperiod=5)
    df['ema10'] = ta.EMA(df, timeperiod=10)
    df['ema21'] = ta.EMA(df, timeperiod=21)
    df['ema50'] = ta.EMA(df, timeperiod=50)
    df['ema100'] = ta.EMA(df, timeperiod=100)

    # SMA - Simple Moving Average
    df['sma3'] = ta.SMA(df, timeperiod=3)
    df['sma5'] = ta.SMA(df, timeperiod=5)
    df['sma10'] = ta.SMA(df, timeperiod=10)
    df['sma21'] = ta.SMA(df, timeperiod=21)
    df['sma50'] = ta.SMA(df, timeperiod=50)
    df['sma100'] = ta.SMA(df, timeperiod=100)

    hilbert = ta.HT_SINE(df)
    df['htsine'] = hilbert['sine']
    df['htleadsine'] = hilbert['leadsine']

    # TODO Add more indicators
    df['adx'] = ta.ADX(df)
    df['adxr'] = ta.ADXR(df)
    df['apo'] = ta.APO(df)
    aroon = ta.AROON(df)
    df['aroonup'] = aroon['aroonup']
    df['aroondown'] = aroon['aroondown']
    df['aroonosc'] = ta.AROONOSC(df)
    df['bop'] = ta.BOP(df)
    df['cci'] = ta.CCI(df)
    df['cmo'] = ta.CMO(df)
    df['dx'] = ta.DX(df)
    macd = ta.MACD(df)
    df['macd'] = macd['macd']
    df['macdsignal'] = macd['macdsignal']
    df['macdhist'] = macd['macdhist']
    df['mfi'] = ta.MFI(df)
    df['minus_di'] = ta.MINUS_DI(df)
    df['minus_dm'] = ta.MINUS_DM(df)
    df['mom'] = ta.MOM(df)
    df['plus_di'] = ta.PLUS_DI(df)
    df['plus_dm'] = ta.PLUS_DM(df)
    df['ppo'] = ta.PPO(df)
    df['roc'] = ta.ROC(df)
    df['rocp'] = ta.ROCP(df)
    df['rocr'] = ta.ROCR(df)
    df['rocr100'] = ta.ROCR100(df)
    df['rsi'] = ta.RSI(df)
    rsi = 0.1 * (df['rsi'] - 50)
    df['fisher_rsi'] = (np.exp(2 * rsi) - 1) / (np.exp(2 * rsi) + 1)
    df['fisher_rsi_norma'] = 50 * (df['fisher_rsi'] + 1)
    stoch = ta.STOCH(df)
    df['slowd'] = stoch['slowd']
    df['slowk'] = stoch['slowk']
    stoch_fast = ta.STOCHF(df)
    df['fastd'] = stoch_fast['fastd']
    df['fastk'] = stoch_fast['fastk']
    stoch_rsi = ta.STOCHRSI(df)
    df['fastd_rsi'] = stoch_rsi['fastd']
    df['fastk_rsi'] = stoch_rsi['fastk']
    df['sar'] = ta.SAR(df)
    df['tema'] = ta.TEMA(df, timeperiod=9)
    df['trix'] = ta.TRIX(df)
    df['ultosc'] = ta.ULTOSC(df)
    df['willr'] = ta.WILLR(df)

    # Pattern Recognition - Bullish candlestick patterns
    # ------------------------------------
    # Hammer: values [0, 100]
    df['CDLHAMMER'] = ta.CDLHAMMER(df)
    # Inverted Hammer: values [0, 100]
    df['CDLINVERTEDHAMMER'] = ta.CDLINVERTEDHAMMER(df)
    # Dragonfly Doji: values [0, 100]
    df['CDLDRAGONFLYDOJI'] = ta.CDLDRAGONFLYDOJI(df)
    # Piercing Line: values [0, 100]
    df['CDLPIERCING'] = ta.CDLPIERCING(df) # values [0, 100]
    # Morningstar: values [0, 100]
    df['CDLMORNINGSTAR'] = ta.CDLMORNINGSTAR(df) # values [0, 100]
    # Three White Soldiers: values [0, 100]
    df['CDL3WHITESOLDIERS'] = ta.CDL3WHITESOLDIERS(df) # values [0, 100]

    # Pattern Recognition - Bearish candlestick patterns
    # ------------------------------------
    # Hanging Man: values [0, 100]
    df['CDLHANGINGMAN'] = ta.CDLHANGINGMAN(df)
    # Shooting Star: values [0, 100]
    df['CDLSHOOTINGSTAR'] = ta.CDLSHOOTINGSTAR(df)
    # Gravestone Doji: values [0, 100]
    df['CDLGRAVESTONEDOJI'] = ta.CDLGRAVESTONEDOJI(df)
    # Dark Cloud Cover: values [0, 100]
    df['CDLDARKCLOUDCOVER'] = ta.CDLDARKCLOUDCOVER(df)
    # Evening Doji Star: values [0, 100]
    df['CDLEVENINGDOJISTAR'] = ta.CDLEVENINGDOJISTAR(df)
    # Evening Star: values [0, 100]
    df['CDLEVENINGSTAR'] = ta.CDLEVENINGSTAR(df)

    # Pattern Recognition - Bullish/Bearish candlestick patterns
    # ------------------------------------
    # Three Line Strike: values [0, -100, 100]
    df['CDL3LINESTRIKE'] = ta.CDL3LINESTRIKE(df)
    # Spinning Top: values [0, -100, 100]
    df['CDLSPINNINGTOP'] = ta.CDLSPINNINGTOP(df) # values [0, -100, 100]
    # Engulfing: values [0, -100, 100]
    df['CDLENGULFING'] = ta.CDLENGULFING(df) # values [0, -100, 100]
    # Harami: values [0, -100, 100]
    df['CDLHARAMI'] = ta.CDLHARAMI(df) # values [0, -100, 100]
    # Three Outside Up/Down: values [0, -100, 100]
    df['CDL3OUTSIDE'] = ta.CDL3OUTSIDE(df) # values [0, -100, 100]
    # Three Inside Up/Down: values [0, -100, 100]
    df['CDL3INSIDE'] = ta.CDL3INSIDE(df) # values [0, -100, 100]

    df["RSI"] = ta.RSI(df["close"], timeperiod = 14)
    df["ROC"] = ta.ROC(df["close"], timeperiod = 10)
    df["%R"] = ta.WILLR(df["high"], df["low"], df["close"], timeperiod = 14)
    df["OBV"] = ta.OBV(df["close"], df["volume"])
    df["MACD"], df["MACD_SIGNAL"], df["MACD_HIST"] = ta.MACD(df["close"], fastperiod=12, slowperiod=26, signalperiod=9)
    
    df["Prediction"] = np.where(df["close"].shift(-5) > df["close"], 1, -1)

    # Remove na values
    df.dropna(inplace=True)

    if Mode == 0:
        df.to_csv('./KI/Data/CurrencyPair_{}/Sorted.csv'.format(CurrencyPair), index=False)
    else:
        df.to_csv('./KI/Actual/CurrencyPair_{}/Sorted.csv'.format(CurrencyPair), index=False)

def check(CurrencyPairList):
    model = load('./KI/Models/model.joblib')
    print("Model loaded")
    while True:
        timer = datetime.datetime.now().minute
        if timer == 00 or timer == 15 or timer == 30 or timer == 45:
            time.sleep(10)
            helper.trade.check_order()
            print(datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S"))
            for CurrencyPair in CurrencyPairList:
                # print(CurrencyPair)
                data = helper.binance.get_klines(CurrencyPair)
                with open('./KI/Actual/CurrencyPair_{}/Data.csv'.format(CurrencyPair), 'w', newline = '') as f:
                    writer = csv.writer(f)
                    writer.writerow(['Open time', 'open', 'high', 'low', 'close', 'volume', 'Close time', 'Quote asset volume', 'Number of trades', 'Taker buy base asset volume', 'Taker buy quote asset volume', 'Ignore'])
                    for line in data:
                        writer.writerow(line)
                    # Sort Data
                # print("Sort Data" + CurrencyPair)
                sort_data(CurrencyPair, 1)

                df = pd.read_csv('./KI/Actual/CurrencyPair_{}/Sorted.csv'.format(CurrencyPair))


                X = df[["Bodysize", "Shadowsize", "RSI", "ROC", "OBV","%R", "MACD", "MACD_SIGNAL", "MACD_HIST", "Modulo_10", "Modulo_100", "Modulo_1000", "Modulo_500", "Modulo_50", "ema3", "ema5", "ema10", "ema21", "ema50", "ema100", "sma3", "sma5", "sma10", "sma21", "sma50", "sma100", "htsine", "htleadsine", "adx", "adxr", "apo", "aroonup", "aroondown", "aroonosc", "bop", "cci", "cmo", "dx", "macd", "macdsignal", "macdhist", "mfi", "minus_di", "minus_dm", "mom", "plus_di", "plus_dm", "ppo", "roc", "rocp", "rocr", "rocr100", "fisher_rsi", "fisher_rsi_norma", "slowd", "slowk", "fastd", "fastk", "fastd_rsi", "fastk_rsi", "sar", "tema", "trix", "ultosc", "willr", "CDLHAMMER", "CDLINVERTEDHAMMER", "CDLDRAGONFLYDOJI", "CDLPIERCING", "CDLMORNINGSTAR", "CDL3WHITESOLDIERS", "CDLHANGINGMAN", "CDLSHOOTINGSTAR", "CDLGRAVESTONEDOJI", "CDLDARKCLOUDCOVER", "CDLEVENINGDOJISTAR", "CDLEVENINGSTAR", "CDL3LINESTRIKE", "CDLSPINNINGTOP", "CDLENGULFING", "CDLHARAMI", "CDL3OUTSIDE", "CDL3INSIDE"]]
                pred = model.predict(X)
                if not-1 in pred[-3:]:
                    print("!!!BUY!!! " + CurrencyPair)
                    if conf['buyandsell'] == 'True':
                        helper.trade.buy(CurrencyPair)
            time.sleep(60)
        time.sleep(15)

def backtesting(CurrencyPairList):
    print("Backtesting")
    
    print("Number of cpu : ", multiprocessing.cpu_count())
    # p = multiprocessing.Pool(multiprocessing.cpu_count())
    p = multiprocessing.Pool(int(int(conf['backtestingpools'])/2))
    for CurrencyPair in CurrencyPairList:
        p.apply_async(backtestprocess, args=(CurrencyPair,))
        time.sleep(600)
    p.close()
    p.join()

def backtestprocess(CurrencyPair):
    takeprofitList = np.arange(0.01,0.1,0.01)
    stoplossList = np.arange(0.01,0.3,0.01)
    print("Backtesting: " + CurrencyPair, flush=True)
    model = load('./KI/Models/model.joblib')
    df = pd.read_csv('./KI/Data/CurrencyPair_{}/Sorted.csv'.format(CurrencyPair))
    dict = {}
    
    for takeprofit in takeprofitList:
        for stoploss in stoplossList:
            money = 100.0
            count_profit = 0
            count_loss = 0
            buy_price = -1
            print("Backtesting: " + CurrencyPair + " Takeprofit: " + str(takeprofit) + " Stoploss: " + str(stoploss), flush=True)
            for ind in df.index:
                if ind > 500:
                    if buy_price == -1:
                        data = pd.DataFrame().reindex_like(df)
                        data = data.drop(range(0,len(df.index)))
                        for x in range(ind-500,ind):
                            data = data.append(df.iloc[x])
                        X = data[["Bodysize", "Shadowsize", "RSI", "ROC", "OBV","%R", "MACD", "MACD_SIGNAL", "MACD_HIST", "Modulo_10", "Modulo_100", "Modulo_1000", "Modulo_500", "Modulo_50", "ema3", "ema5", "ema10", "ema21", "ema50", "ema100", "sma3", "sma5", "sma10", "sma21", "sma50", "sma100", "htsine", "htleadsine", "adx", "adxr", "apo", "aroonup", "aroondown", "aroonosc", "bop", "cci", "cmo", "dx", "macd", "macdsignal", "macdhist", "mfi", "minus_di", "minus_dm", "mom", "plus_di", "plus_dm", "ppo", "roc", "rocp", "rocr", "rocr100", "fisher_rsi", "fisher_rsi_norma", "slowd", "slowk", "fastd", "fastk", "fastd_rsi", "fastk_rsi", "sar", "tema", "trix", "ultosc", "willr", "CDLHAMMER", "CDLINVERTEDHAMMER", "CDLDRAGONFLYDOJI", "CDLPIERCING", "CDLMORNINGSTAR", "CDL3WHITESOLDIERS", "CDLHANGINGMAN", "CDLSHOOTINGSTAR", "CDLGRAVESTONEDOJI", "CDLDARKCLOUDCOVER", "CDLEVENINGDOJISTAR", "CDLEVENINGSTAR", "CDL3LINESTRIKE", "CDLSPINNINGTOP", "CDLENGULFING", "CDLHARAMI", "CDL3OUTSIDE", "CDL3INSIDE"]]

                        pred = model.predict(X)
                        if not-1 in pred[-5:]:
                            print("!!!BUY!!! " + CurrencyPair)
                            print(df["close"][ind])
                            buy_price = df["close"][ind]
                    if df["high"][ind] > buy_price + (buy_price * takeprofit) and buy_price > -1:
                        print("+++SELL+++ " + CurrencyPair)
                        money = money + (10 * takeprofit)
                        print("Result: " + str(money))
                        buy_price = -1
                        count_profit += 1
                    if df["low"][ind] < buy_price - (buy_price * stoploss) and buy_price > -1:
                        print("---SELL--- " + CurrencyPair)
                        money = money - (10 * stoploss)
                        print("Result: " + str(money))
                        buy_price = -1
                        count_loss += 1
            dict[str(takeprofit)] = {}
            dict[str(takeprofit)][str(stoploss)] = {}
            dict[str(takeprofit)][str(stoploss)]['result'] = money
            dict[str(takeprofit)][str(stoploss)]['count_profit'] = count_profit
            dict[str(takeprofit)][str(stoploss)]['count_loss'] = count_loss
            print("CurrencyPair: " + CurrencyPair)
            print("Result: " + str(money))
            print("Takeprofit: " + str(takeprofit))
            print("Stoploss : " + str(stoploss))
            with open("./KI/Results/{}.json".format(CurrencyPair), "w") as outfile:
                json.dump(dict, outfile)
