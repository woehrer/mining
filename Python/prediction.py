import csv
import os
import warnings
from datetime import datetime
import multiprocessing

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score

import helper.binance
import helper.training
import helper.config
from joblib import dump

warnings.filterwarnings('ignore')

conf = helper.config.initconfig()



CurrencyPairList = ["BTCUSDT","ETHUSDT","BNBUSDT","XMRUSDT","ATOMUSDT","LUNCUSDT","ADAUSDT","AMBUSDT","XRPUSDT","SOLUSDT","DOGEUSDT","DOTUSDT","MATICUSDT","ERNUSDT","SHIBUSDT","SUNUSDT","LUNAUSDT"]
# CurrencyPairList = ["BTCUSDT","ETHUSDT","BNBUSDT","XMRUSDT","ATOMUSDT","LUNCUSDT","ADAUSDT","AMBUSDT"]


if not os.path.exists('./KI'):
        os.makedirs('./KI')

if not os.path.exists('./KI/Data'):
        os.makedirs('./KI/Data')

if not os.path.exists('./KI/Actual'):
        os.makedirs('./KI/Actual')

if not os.path.exists('./KI/Models'):
        os.makedirs('./KI/Models')

if not os.path.exists('./KI/Results'):
        os.makedirs('./KI/Results')

for CurrencyPair in CurrencyPairList:
    if not os.path.exists('./KI/Data/CurrencyPair_{}'.format(CurrencyPair)):
        os.makedirs('./KI/Data/CurrencyPair_{}'.format(CurrencyPair))

    if not os.path.exists('./KI/Actual/CurrencyPair_{}'.format(CurrencyPair)):
        os.makedirs('./KI/Actual/CurrencyPair_{}'.format(CurrencyPair))


if conf['request_data'] == 'True':

    p = multiprocessing.Pool(int(conf['backtestingpools']))

    for CurrencyPair in CurrencyPairList:

        print("Request Data " + CurrencyPair)

        data = helper.binance.get_historical_klines(CurrencyPair)

        # Start to Save Data
        # Open time / Open / High / Low / Close / Volume / Close time / Quote asset volume / Number of trades / Taker buy base asset volume / Taker buy quote asset volume / Ignore
        print("Save Data" + CurrencyPair)
        with open('./KI/Data/CurrencyPair_{}/Data.csv'.format(CurrencyPair), 'w', newline = '') as f:
            writer = csv.writer(f)
            writer.writerow(['Open time', 'open', 'high', 'low', 'close', 'volume', 'Close time', 'Quote asset volume', 'Number of trades', 'Taker buy base asset volume', 'Taker buy quote asset volume', 'Ignore'])
            for line in data:
                writer.writerow(line)

        # Sort Data
        print("Sort Data" + CurrencyPair)
        # helper.training.sort_data(CurrencyPair, 0)
        p.apply_async(helper.training.sort_data, args=(CurrencyPair, 0,))
    p.close()
    p.join()

if conf['train'] == 'True':

    df = helper.training.concat(CurrencyPairList)


    #Splitting the data into X and y variables and perform a train test split
    from sklearn.model_selection import train_test_split

    X = df[["Bodysize", "Shadowsize", "RSI", "ROC", "OBV","%R", "MACD", "MACD_SIGNAL", "MACD_HIST", "Modulo_10", "Modulo_100", "Modulo_1000", "Modulo_500", "Modulo_50", "ema3", "ema5", "ema10", "ema21", "ema50", "ema100", "sma3", "sma5", "sma10", "sma21", "sma50", "sma100", "htsine", "htleadsine", "adx", "adxr", "apo", "aroonup", "aroondown", "aroonosc", "bop", "cci", "cmo", "dx", "macd", "macdsignal", "macdhist", "mfi", "minus_di", "minus_dm", "mom", "plus_di", "plus_dm", "ppo", "roc", "rocp", "rocr", "rocr100", "fisher_rsi", "fisher_rsi_norma", "slowd", "slowk", "fastd", "fastk", "fastd_rsi", "fastk_rsi", "sar", "tema", "trix", "ultosc", "willr", "CDLHAMMER", "CDLINVERTEDHAMMER", "CDLDRAGONFLYDOJI", "CDLPIERCING", "CDLMORNINGSTAR", "CDL3WHITESOLDIERS", "CDLHANGINGMAN", "CDLSHOOTINGSTAR", "CDLGRAVESTONEDOJI", "CDLDARKCLOUDCOVER", "CDLEVENINGDOJISTAR", "CDLEVENINGSTAR", "CDL3LINESTRIKE", "CDLSPINNINGTOP", "CDLENGULFING", "CDLHARAMI", "CDL3OUTSIDE", "CDL3INSIDE"]]
    y = df[["Prediction"]]


    print("Trainset Split")
    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state = 42, train_size = 0.8, test_size=0.2)

    #Creating the model with the Random Forest classifier, choosing the gini criterion 

    model = RandomForestClassifier(n_estimators = 2000, oob_score = True, criterion = "gini", random_state=0, n_jobs=int(conf['backtestingpools']), verbose=1)


    print("Fit")
    model.fit(X_train, y_train)

    print("Predict")
    y_pred = model.predict(X_test)

    accuracy = accuracy_score(y_test, y_pred, normalize= True)*100

    print("Correct prediction in %: ", accuracy)

#     # localfilePath = './KI/Models/model'
#     # model.save(localfilePath)
    dump(model, './KI/Models/model.joblib', compress = 3, )
#     # model = load('./KI/Models/model.joblib')

    print("Model saved")
